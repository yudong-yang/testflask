import hashlib
import json
import requests
from datetime import datetime
import time

class OpenPlatformDemo(object):

    def __init__(self):
        '''
        开发者应用账号，由猫酷提供
        '''
        # self.appID = '5f0e74bc4ba1f21d2cd452af'  #  AppID
        # self.publicKey = '5UOErf'  # 公钥
        # self.privateKey = 'cd007c6405f874df'  # 私钥

        # //北京荟聚
        #    //北京荟聚配置
        # rPSvbj
        # a3bdd7850c8ebde0
        # 5f6c399c4bae7047ace8d0aa

        # {
        #     "appId": 76025,
        #     "developerAppId": "5f0d8e504bae7051c88ad953",
        #     "developerPublicKey": "zkQMJB",
        #     "developerPrivateKey": "b0eb52550c31fc6c"
        ## }
        # #北京荟聚：
        # self.appID = '5f6c399c4bae7047ace8d0aa'  # AppID
        # self.publicKey = 'rPSvbj'  # 公钥
        # self.privateKey = 'a3bdd7850c8ebde0'  # 私钥

        #无锡荟聚
       # self.appID = '5f0d8e504bae7051c88ad953'  # AppID
       # self.publicKey = '-Ham7p'  # 公钥
      #  self.privateKey = '152fe116c89bc527'  # 私钥


        self.appID = '614444f5812f2c29e4f824b8'  # AppID
        self.publicKey = 'QiQRVx'  # 公钥
        self.privateKey = '9d406cc18d0c05e7'  # 私钥

        # APPID: 5f6c399c4bae7047ace8d0aa
        # 私钥: a3bdd7850c8ebde0
        # 公钥: rPSvbj


        # self.appID = '5f6c399c4bae7047ace8d0aa'  # AppID
        # self.publicKey = 'rPSvbj'  # 公钥
        # self.privateKey = 'a3bdd7850c8ebde0'  # 私钥

    def mallcoo_post(self, url, json_data):
        '''
        :param url: 请求url
        :param post_data: 请求参数（json格式）
        :return: Response object
        '''

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')  # 时间戳

        encryptString = "{publicKey:" + self.publicKey + ",timeStamp:" + timestamp + ",data:" + json.dumps(
            json_data) + ",privateKey:" + self.privateKey + "}"  # 待加密字符串
        print("签名源串"+encryptString)
        sign = hashlib.md5(encryptString.encode(encoding='utf-8')).hexdigest()[8:24].upper()  # 16位MD5加密（大写）
        print("签名=："+sign)
        headers = {
            'Content-Type': 'application/json;charset=utf-8',
            'AppID': self.appID,
            'PublicKey': self.publicKey,
            'TimeStamp': timestamp,
            'Sign': sign
        }
        print("请求=" ,json_data)
        response = requests.post(url, json=json_data, headers=headers)

        return response


if __name__ == '__main__':
    '''
        获取商户详情
        :return:
    '''


    def get_userToken():
        url = ' https://openapi10.mallcoo.cn/User/OAuth/v1/GetToken/ByTicket/'
        json_data = {
            "Ticket": "9d507db6b83d3ce4"
        }

        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')

    def get_userInfo():
        url="https://openapi10.mallcoo.cn/User/AdvancedInfo/v1/Get/ByToken/"
        json_data={
            "UserToken":"fd3ce296d8360211"
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')


    def get_vip():
        url = "https://openapi10-t.mallcoo.cn/Custom/huiju/v1/BindMallCardOpen/"
        json_data = {
            "CardNo": "SHKPIC00017289"
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')


    def add_credits():
        url = "https://openapi10.mallcoo.cn/User/Score/v1/Plus/ByMobile/"
        json_data={
          "Mobile": "18258149680",
          "Score": 1,
          "TransID": int(time.time() * 1000),
          "ScoreEvent": 2000,
          "Reason":"%e7%a7%af%e5%88%86%e5%95%86%e5%9f%8e%e6%89%a3%e7%a7%af%e5%88%86"
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')


    def sub_credits():
        url = "https://openapi10.mallcoo.cn/User/Score/v1/Subtract/ByMobile/"
        json_data = {
            "Mobile": "18258149680",
            "Score": 1,
            "TransID": int(time.time() * 1000),
            "ScoreEvent": 2000,
            "Reason": "扣积分描述！！"
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')


    def get_coupen_list():
        url = "https://openapi10.mallcoo.cn/Coupon/PutIn/v3/GetAll/"
        json_data = {
            "PageIndex": 1,
            "PageSize": 10
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')


    def get_credits():
        url = "https://openapi10.mallcoo.cn/User/Score/v1/Get/Records/ByMobile/"
        json_data = {
            "MallCardNo":"05101002010752",
            "Mobile": "18258149680",
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')

    def send_message():
        url = 'https://openapi10.mallcoo.cn/Message/Weixin/v1/SendTM/'
        message_data = {
            "first": "订单发货通知。",
            "keyword1": "测试兑换商品，、",
            "keyword2": "12121212",
            "keyword3": "2020-10-27",
            "remark": "请去订单详情页面查看订单最新状态！"
        }
        json_data = {
            "MallCardNo": "",
            "Mobile": "18258149680",
            "Data": json.dumps(message_data),
           # "TemplateID": "s-TZkrlszW7iYtMKRwFb7aI622J1TXTrKXdvX_vkpFg",#北京
            "TemplateID": "thjm_y498fWMRtTXmKW-enrpSL9DSdPL-n-8a3iKPQM",#无锡
            "Url": "https://huiju.activity-1.m.duiba.com.cn/customShare/share?id=4218"
        }
        demo = OpenPlatformDemo()
        response = demo.mallcoo_post(url, json_data)
        print(response.status_code, response.text, sep='\n')

#0JtCvtTJU6NTLAljIs_q2uaghkc2mtxD0v04rhokhJs
#  thjm_y498fWMRtTXmKW-enrpSL9DSdPL-n-8a3iKPQM 通知模板
get_vip()
#05101002010752
#：北京：TZkrlszW7iYtMKRwFb7aI622J1TXTrKXdvX_vkpFg
#北京：dcnsiJjTzFUXoBX_iASBAJ8bHHwD9_UYNiTXLALI3_s
#无锡：thjm_y498fWMRtTXmKW-enrpSL9DSdPL-n-8a3iKPQM