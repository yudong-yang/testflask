import hashlib
import time
import requests
import json
import random

def sign(params):
    '对参数进行签名验证'
    sign_str=''
    keys=sorted(params.keys())
    for key in keys:
        sign_str=sign_str + key +'=' + str(params[key])+'&'
    sign = hashlib.sha256(sign_str[:-1].encode("utf8")).hexdigest()
    return sign

def build_nonceStr():
    strs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'
    sub_str = ''
    for i in range(0,8):
        index = random.randint(0, len(strs)-1)
        sub_str = sub_str+strs[index]
    return sub_str


#appId='7f701174bf8a4cb681cb58b9db995429'
#secret='51a1d4d30bf948eb84b2b3cc1381b5da'
appId='d86c8e0de97f486fbd4d929d3fc774be'
secret='4ebc324fbccb465ea407c3ace46e448b'
nonceStr=build_nonceStr()
timestamp=int(time.time())
print(nonceStr)
print(timestamp)
params1 = {}
params1['appId']=appId
params1['nonceStr']=nonceStr
params1['secret']=secret
params1['timestamp']=timestamp
signature = sign(params1)
print(signature)

url = 'https://open.95516.com/open/access/1.0/backendToken?'
#url = 'https://open.95516.com/open/access/1.0/frontToken'
post_data = {}
post_data['appId']=appId
post_data['nonceStr']=nonceStr
post_data['timestamp']=timestamp
post_data['signature']=signature
jsons = json.dumps(post_data)
texts = requests.post(url,data=jsons)
print(texts.json())
frontToken=texts.json()['params']['backendToken']
signurl = 'https://activity.m.duiba.com.cn/customShare/share?id=3845'
signparams = {}
signparams['appId']=appId
signparams['nonceStr']=nonceStr
signparams['timestamp']=timestamp
signparams['url']=signurl
signparams['frontToken']=frontToken

str2 = sign(signparams)
print(str2)

