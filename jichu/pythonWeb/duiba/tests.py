import requests
import hashlib
import time
import json

#山东农行接口测试

def get_md5_value(src):
    myMd5 = hashlib.md5()
    myMd5.update(src.encode("utf-8"))
    myMd5_Digest = myMd5.hexdigest()
    return myMd5_Digest.upper()
#duiba20191119155700appIdduiba20191119155700timestamp1577700186040duiba20191119155700


def buildSignString(appSecret , params):
    signStr = appSecret
    for key, value in params.items():
        signStr = signStr+key+str(value)
    return signStr+appSecret

def buildPost(appId,appSecret,params):
    signParams = {}
    signParams['appId'] = appId
    signParams['timestamp'] = int(time.time() * 1000)

    signstr = buildSignString(appSecret, signParams)
    sign = get_md5_value(signstr)
    newParam={}
    newParam.update(params,**signParams)
    newParam["sign"] = sign
    return newParam


url = 'https://wxtestsd.abchina.com/sdwx5/bank/wx/third/checkAny'
params = {}
data = {"openId": "oUQVlwDDRUH2XxILLuAV9VMHbTVg",
        "groupIdS": "201810160001",
        "orgId": "15H999"}
params["data"] = data
datas = buildPost("duiba20191119155700","duiba20191119155700",params)
r= requests.post(url,json.dumps(datas),headers={'Content-Type': 'application/json'})
print(r.text)





