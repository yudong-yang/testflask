import requests
import json
import hashlib
import time




def buildPush(sk,name,userList):
    timestamp = int(time.time())
    sign = getSign(sk,name,timestamp)
    params = {
        'time':timestamp,
        'sign':sign,
        'push_data':userList
    }

    return params


def buildarticle(sk,name,userid):
    timestamp = int(time.time())
    sign = getSign(sk, name, timestamp)
    params={
        'time': timestamp,
        'sign': sign,
        'user_hid':userid
    }
    return params


def getSign(sk,name,timestamp):
    '对参数进行签名验证'
    sign_str = sk+name+str(timestamp)
    sign = get_md5_value(sign_str)
    return sign


def get_md5_value(src):
        myMd5 = hashlib.md5()
        myMd5.update(src.encode("utf-8"))
        myMd5_Digest = myMd5.hexdigest()
        print(myMd5_Digest)
        return myMd5_Digest



sk='46476e852a5c47bc9f82985726f7c186'
name='兑吧活动'
userList=[{"user_hid":"123123","title":"测试push标题","content":"测试push内容","link":"http://www.baidu.com"}]

headers = {
    'Ak': '01bc3ed81bd9f1acfda3869e6712c91e',
    'App-Version': '1.0.0',
    'Device': 'duiba'
}
url = 'https://pre-api.bhmc.com.cn/v1/app/channel/white/duiba/batch_push'
url2= 'https://pre-api.bhmc.com.cn/v1/app/channel/white/duiba/user_article'
params = buildPush(sk,name,userList)
params2 = buildarticle(sk,name,'c76d42846ac94d33aefe6038208a7e94')
print(params2)
resp2 = requests.get(url2,params=params2,headers=headers)
#resp = requests.post(url,json=params,headers=headers)
print(resp2.text)





#测试账号
# user_hid:  ae2a91de09c94ab5a5eff43f1f8f3b89          手机号： 16140513501
# user_hid:  045721dd0f0d4b8c94479004ebed6c39      手机号： 13512312325
# user_hid:  1ad7c69ff6dd4e8a9ed248ef4e89e13b        手机号： 13612312332