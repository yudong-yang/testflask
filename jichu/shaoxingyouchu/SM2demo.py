from gmssl.sm2utils import PrivateKey
from gmssl import sm2, func
from gmssl import SM3hash

def test_sm2():

    #private_key = '00B9AB0B828FF68872F21A837FC303668428DEA11DCD1B24429D0C99E24EED83D5'
    private_key = 'D25595C27BC0E4C678533F06D9D7BA66EECDBCED47268112B48E5CEA4563EC00'
    #public_key = 'B9C9A6E04E9C91F7BA880429273747D7EF5DDEB0BB2FF6317EB00BEF331A83081A6994B8993F3F5D6EADDDB81872266C87C018FB4162F5AF347B483E24620207'
    public_key = 'E0E2E37F11901483CDFBC47F489D87D5D78C55DD7F919B73DEA83007748668B7871A1BA9608F156E25B7D64C7821379BAC1E2C591D5A50FF311D1AAE026C1DAE'

    sm2_crypt = sm2.CryptSM2(
        public_key=public_key, private_key=private_key,mode=1)
    data = "asdf"
    enc_data = sm2_crypt.encrypt(data.encode('utf8'))
    print("enc_data:%s" % enc_data.hex())
    #print("enc_data_base64:%s" % base64.b64encode(bytes.fromhex(enc_data)))
    descdata='86D052D807F54C8796BF16EEE92EABD9C0FB6EFB7D3CE663AEA248BF564D09641007E71E7BDD2AE5EEBE36C8E7AFA5DF898CBB505B05EF05028898EF081BEECD9B0CA38E8B9C846AB55579FE120ACAC9CC14A790D864EA8AA5CE6F70C5D684806AC69BE4'
    hexstrs = bytes.fromhex(descdata)
    dec_data = sm2_crypt.decrypt(hexstrs)
    print("dec_data:%s" % dec_data.decode())
    assert data.encode('utf8') == dec_data

    print("-----------------test sign and verify---------------")
    random_hex_str = func.random_hex(sm2_crypt.para_len)
    hexdata=SM3hash.sm3_hash(b'asdf')
    print("hash=:%s",hexdata)
    sign = sm2_crypt.sign(hexdata.encode("utf8"),random_hex_str)
    print('sign:%s' % sign)
    ensign="d295b703be60a3c7c4e355be355c7de545ea800e2856a82393c19a3aab9b28f872313365a56d0fe5f44b9d211f17a4755611e4aca9ca1bc373a06968032a2621"
    verify = sm2_crypt.verify(ensign ,hexdata.encode("utf8"))
    print('verify:%s' % verify)
    assert verify


def test_sm3():
     private_key = 'D25595C27BC0E4C678533F06D9D7BA66EECDBCED47268112B48E5CEA4563EC00'
     public_key = 'E0E2E37F11901483CDFBC47F489D87D5D78C55DD7F919B73DEA83007748668B7871A1BA9608F156E25B7D64C7821379BAC1E2C591D5A50FF311D1AAE026C1DAE'

     random_hex_str = "59276E27D506861A16680F3AD9C02DCCEF3CC1FA3CDBE4CE6D54B80DEAC1BC21"

     sm2_crypt = sm2.CryptSM2(public_key=public_key, private_key=private_key)
     data = b"asdfgh"
     print("-----------------test SM2withSM3 sign and verify---------------")
     sign = sm2_crypt.sign_with_sm3(data, random_hex_str)
     print('sign: %s' % sign)
     verify = sm2_crypt.verify_with_sm3(sign, data)
     print('verify: %s' % verify)
     assert verify

if __name__ == '__main__':
    priKey = PrivateKey()
    pubKey = priKey.publicKey()
    print(priKey.toString())
    print(pubKey.toString(compressed = False))
    test_sm2()
    print("==="*20)
    test_sm3()
