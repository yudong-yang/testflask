

from pysmx.SM2 import generate_keypair
from pysmx.SM2 import Sign
from pysmx.SM2 import Verify

pk, sk = generate_keypair()
pkstr = pk.hex()
skstr = sk.hex()
print(pkstr)
print(skstr)
dbsk=bytes.fromhex(skstr)
dbpk=bytes.fromhex(pkstr)
print(dbsk)
print(dbpk)
kks='2E7B86E7798879D48EB0EFF7250156BE5AB720459149EE5206A6011141D666D0'
pks='04F5B09EEDCFF97A17E367578ACAAE80ADAE62A9CF1211681903F9F9BC82FCF8B5465E54B1BB6C1E5C38AC6D48ACD7226F5F0D0ABAA1765BD3F746D318BFE7BA0B'
len_para = 64
sig = Sign("112233", bytes.fromhex(kks), '1234567812345678', len_para)
print(Verify(sig, "112233", bytes.fromhex(pks), len_para))
print(sig.hex())