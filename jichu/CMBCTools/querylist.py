import requests
import json
import hashlib
import time
import datetime
from pysmx.SM2 import Sign
#新版appid
appid='aa068f5f-2d35-4fb4-ae0a-5059eff37898'
appSecret='c9520a11-303c-41c5-ac16-34fdef3cada1'

sk='2E7B86E7798879D48EB0EFF7250156BE5AB720459149EE5206A6011141D666D0'



#旧版appid
#appid='abba98ec-34fc-49a8-b7a5-3676a46587b2'
#appSecret='0961f519-e536-433f-8d87-8ed2f62c9ad7'

ProjectId='p20211109gx61xK'
path='https://api.cmbchina.com/quaquery/gatewaynew/api/gateway'
#     https://api.cmbchina.com/quaquery/gateway/api/gateway

def smsign(privatekey,str):
    len_para = 64
    sig = Sign(str, bytes.fromhex(privatekey), '12345678abcdef', len_para)
    return sig.hex()


def get_md5_value(src):
    myMd5 = hashlib.md5()
    myMd5.update(src.encode("utf-8"))
    myMd5_Digest = myMd5.hexdigest()
    return myMd5_Digest

def build_reqbody(qyerytype,queryid,projectId,uid):
    fulltime=datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    infboylis = {}
    if qyerytype=='group':
        infboylis['QUA_GRP_ID'] = queryid
        infboylis['QRY_TYP'] = 'G'
    if qyerytype=='id':
        infboylis['QUA_ID'] = queryid
        infboylis['QRY_TYP'] = 'D'
    infboylis['QRY_CHN'] = '06'
    infboylis['UNI_USER_ID'] = uid
    infboylis['QRY_ORG'] = projectId
    body={}
    body['PRCCOD']='QPQ21V01'
    body['ISUDAT']=fulltime.split('-')[0]
    body['ISUTIM']=fulltime.split('-')[1]
    infboy={}
    infboy['QPQ21V01X1']=[infboylis]
    body['INFBDY']=infboy
    return json.dumps(body)

def get_query_result(qyerytype,queryid,projectId,uid):
    body=build_reqbody(qyerytype,queryid,projectId,uid)
    reqbody=json.loads(body)
    timestamp=int(time.time())
    signstr = 'appid='+appid+'&secret='+appSecret+'&sign='+body+'&timestamp='+str(timestamp)
    apisign = get_md5_value(signstr)
    apisign2=smsign(sk,signstr)
    headers={
        'Content-Type':'application/json',
        'appid':appid,
        'sign':body,
        'timestamp':str(timestamp),
        'verify':'Md5Verify',
        'apisign':apisign
    }
    print(headers)
    print(reqbody)
    resp = requests.post(path,json=reqbody,headers=headers)
    return resp.text

body=get_query_result('id','V2G20220331103819004Q01','p20211009KOoVxF','UQWz0U7uRE9Gq3hByQJJ93UfpahY4=')
#body=get_query_result('id','V2G20211125191651537','p20210906GrY5QX','UQwLSJnqqV/S/l5NjjRVMoGjyvVwQ=')
print(body)