import hashlib
import time
import requests
import json
import random

def build_nonceStr():
    strs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'
    sub_str = ''
    for i in range(0,8):
        index = random.randint(0, len(strs)-1)
        sub_str = sub_str+strs[index]
    return sub_str

def sign(params):
    '对参数进行签名验证'
    sign_str=''
    keys=sorted(params.keys())
    for key in keys:
        sign_str=sign_str + key +'=' + str(params[key])+'&'
    sign = hashlib.sha256(sign_str[:-1].encode("utf8")).hexdigest()
    return sign

def get_backToken(appId,secret):
    nonceStr=build_nonceStr()
    timestamp=int(time.time())
    token_params = {}
    token_params['appId']=appId
    token_params['nonceStr']=nonceStr
    token_params['secret']=secret
    token_params['timestamp']=timestamp
    signature = sign(token_params)
    token_params['signature']=signature
    token_params.pop('secret')
    jsons = json.dumps(token_params)
    texts = requests.post('https://open.95516.com/open/access/1.0/backendToken?', data=jsons)
    backendToken = texts.json()['params']['backendToken']
    return backendToken

def get_openId(back_token,code,appId):
    post_params = {}
    post_params['backendToken']=back_token
    post_params['code']=code
    post_params['appId']=appId
    post_params['grantType']='authorization_code'
    jsons = json.dumps(post_params)
    texts = requests.post('https://open.95516.com/open/access/1.0/token?',data=jsons)
    return texts

#print(build_nonceStr())
appId='7f701174bf8a4cb681cb58b9db995429'
secret='51a1d4d30bf948eb84b2b3cc1381b5da'
#back_token = get_backToken(appId,secret)
back_token='4VyTXti/QfqX9lEdK5wePQ=='
code='m/q/up9ETxerJzmilSJFEQ=='
text = get_openId(back_token,code,appId)
print(text.json())