import requests
from bs4 import BeautifulSoup
import time

def purse_url(url,send_headers):
    try:
        html = requests.get(url,headers=send_headers)
        soup = BeautifulSoup(html.content, "html.parser")
        return soup
    except:
        print("解析异常url="+url)

def get_url_list(soup):
    try:
        contents = soup.find("div", {"class": "chapter"}).findAll("li")
        url_list = []
        for content in contents:
            li = content.find("a").attrs['href']
            chapter = content.find("a").get_text()
            url_chapter={'chapter':chapter,'url':'https://m.fpzw.com/book/2993/'+li}
            url_list.append(url_chapter)
        return url_list
    except:
        print("解析异常")

def save_file(datas):
    with open('guoluyinyang.txt', 'a',encoding = 'utf-8') as cs:
        try:
            cs.writelines(datas)
            print('写入完成！')
        except :print('写入异常')

def read_to_save(cont_sopu):
    try:
        content = cont_sopu.find("div",{"id":"nr1"})
        save_file(content.get_text())
    except:print("读写异常了")
    
#/Users/yangyudong/git_python/jichu/spider/guicuideng/guoluyinyang.txt

send_headers = {
    'Referer': 'https://www.fpzw.com/xiaoshuo/2/2993/',
    'Cache-Control': 'no-cache',
    'Host': 'm.fpzw.com',
    'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Mobile Safari/537.36',
    'Cookie': 'Hm_lvt_88e4e8c9d153a3e0efa15a9c7524798c=1586485994; Hm_lpvt_88e4e8c9d153a3e0efa15a9c7524798c=1586486016; jieqiVisitId=article_articleviews%3D2993; Hm_lvt_a7a1cb4f7e784fa41853d89f1ad24ee2=1586486036,1586486071; Hm_lpvt_a7a1cb4f7e784fa41853d89f1ad24ee2=1586486071',
    'Connection': 'keep-alive'
}
url = "https://m.fpzw.com/book/2993/"

soup = purse_url(url,send_headers)
#print(soup)
lists = get_url_list(soup)
lists.reverse()


i=1
for li in lists:
    cont_sopu = purse_url(li.get('url'), send_headers)
    send_headers['Referer']=li.get('url')
    read_to_save(cont_sopu)
    print("第%d章读取" % i)
    time.sleep(2)
    i=i+1
