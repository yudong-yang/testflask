from urllib.parse import parse_qs, urlencode
from urllib.request import urlparse
import hashlib
import time

class CreditsTool:
    appKey = ''
    appSecret=''
    def __init__(self,appKey,appSecret):
        self.appKey=appKey
        self.appSecret=appSecret


    def get_md5_value(self,src):
        myMd5 = hashlib.md5()
        myMd5.update(src.encode("utf-8"))
        myMd5_Digest = myMd5.hexdigest()
        return myMd5_Digest


    def bulid_params( self,uid, credits, **sets):
        params = {}
        params['appKey'] = self.appKey
        params['appSecret'] = self.appKey
        params['timestamp'] = int(time.time() * 1000)
        params['uid'] = uid
        params['credits'] = credits
        for key, value in sets.items():
            params[key] = value
        return params


    def bulid_consume_params(self, userId, credits, **sets):
        params = {}
        params['appKey'] = self.appKey
        params['appSecret'] = self.appKey
        params['timestamp'] = int(time.time() * 1000)
        params['userId'] = userId
        params['addCoin'] = credits
        for key, value in sets.items():
            params[key] = value
        return params


    def bulid_notify_params(self, userId, auditResult, **sets):
        params = {}
        params['appKey'] = self.appKey
        params['appSecret'] = self.appKey
        params['timestamp'] = int(time.time() * 1000)
        params['userId'] = userId
        params['auditResult'] = auditResult
        for key, value in sets.items():
            params[key] = value
        return params


    def sign(self,params):
        '对参数进行签名验证'
        sign_str = ''
        keys = sorted(params.keys())
        for key in keys:
            if key != 'sign':
                sign_str = sign_str + str(params[key])
        sign = self.get_md5_value(sign_str)
        return sign


    def signVerify(self, params):
        params['appSecret'] = self.appSecret
        if params['sign'] == self.sign(params):
            print('签名通过' + self.sign(params))
            return True
        else:
            return False


    def buildUrlWithSign(self,url, params):
        '拼装签名链接地址'
        if '?' in url and not url.endswith('?'):
            url = url + '&'
        elif '?' not in url:
            url = url + '?'
        params['appKey']=self.appKey
        params['appSecret']=self.appSecret
        if params.get('timestamp')==None:
            params['timestamp']=int(time.time()*100000)
        signstr = self.sign(params)
        '将签名串放入列表中'
        params['sign'] = signstr
        '拼接免登陆地址时候，移除appSecret'
        del params['appSecret']
        urlparam = urlencode(params)  # '对参数进行urlencode编码'
        return url + urlparam


    def credits_consurme(self, request_params):
        request_params['appSecret'] = self.appSecret
        if self.appKey != request_params['appKey']:
            raise Exception("appKey not match !")
        elif request_params["timestamp"] == '':
            raise Exception("timestamp can't be null ! ")
        elif self.signVerify(self.appSecret, request_params) == False:
            raise Exception("sign verify fail! ")
        else:
            return request_params


    def credits_virtual(self, request_params):
        request_params['appSecret'] = self.appSecret
        if self.appKey != request_params['appKey']:
            raise Exception("appKey not match !")
        elif request_params["timestamp"] == '':
            raise Exception("timestamp can't be null ! ")
        elif self.signVerify(self.appSecret, request_params) == False:
            raise Exception("sign verify fail! ")
        else:
            return request_params


    def credits_notify(self, request_params):
        request_params['appSecret'] = self.appSecret
        if self.appKey != request_params['appKey']:
            raise Exception("appKey not match !")
        elif request_params["timestamp"] == '':
            raise Exception("timestamp can't be null ! ")
        elif self.signVerify(self.appSecret, request_params) == False:
            raise Exception("sign verify fail! ")
        else:
            return request_params


    def qs(self,url):
        query = urlparse(url).query
        return dict([(k, v[0]) for k, v in parse_qs(query).items()])

