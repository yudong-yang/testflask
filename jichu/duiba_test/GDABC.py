
import requests
import hashlib
from datetime import datetime
import base64


class GuangDongABC(object):

    def __init__(self):
        #查询userFlag参数
        self.key = '97ixiq'
        self.secret = 'l1nmwl7k3hs310u2rnjza2j9kjcjm4nj'
        self.url = "https://open.yidianting.xin/openydt/api/v2/"
        #self.url1 = "https://openapi-test.yidianting.xin/openydt/api/v2/"


        #签约信息
        self.qianyue_key='1qaz@WSX'
        # self.qianyue_key='testabc'
        # self.qianyue_url='http://parking.gdpos.cn/AbcApi/queryABCContractType'
        self.qianyue_url='http://47.98.203.141:8011/AbcApi/queryABCContractType'
        # self.qianyue_outDevId='testabc'
        self.qianyue_outDevId='yidianting'



    def GuangDong_post(self,cmd,params):#获取userFlag参数
        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')  # 时间戳
        sign = self.get_md5_value(self.key + ':' + timestamp + ':' + self.secret)
        Authorization = self.getbase64str(self.key + ':' + timestamp)
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=utf-8',
            'Content-Length': '256',
            'Authorization': Authorization
        }
        fullurl = self.url+cmd+"?sign="+sign
        print(fullurl)
        print(params)
        response=requests.post(fullurl,json=params,headers=headers)
        return response

    def qianyue_post(self,phoneNo):#查询签约用户
        params={'outDevId':self.qianyue_outDevId}
        params['phoneNo']=phoneNo
        signstr = 'phoneNo='+phoneNo+'&outDevId='+self.qianyue_outDevId+'&key='+self.qianyue_key
        sign = self.get_md5_value(signstr)
        params['sign']=sign
        print(params)
        response = requests.post(self.qianyue_url,json=params)
        return response

    def getbase64str(self,strs):
        return base64.b64encode(strs.encode()).decode()

    def get_md5_value(self,src):
        myMd5 = hashlib.md5()
        myMd5.update(src.encode("utf-8"))
        myMd5_Digest = myMd5.hexdigest()
        return myMd5_Digest





if __name__ == '__main__':
   cmd = "getYdtUserFlagByPhone"
   params = {"phone": "18258149680"}
   demo = GuangDongABC()
   response = demo.GuangDong_post(cmd,params)
   print(response.status_code, response.text, sep='\n')
   # phoneNo='18258149680'
   # qianyueresp = demo.qianyue_post(phoneNo)
   # print(qianyueresp.status_code,qianyueresp.text, sep='\n')