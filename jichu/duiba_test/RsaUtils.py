import rsa
import base64

def create_keys():  # 生成公钥和私钥
    (pubkey, privkey) = rsa.newkeys(1024)
    #输出公钥文件
    pub = pubkey.save_pkcs1()
    with open('public.pem', 'wb+')as f:
        f.write(pub)
    # 输出私钥文件
    pri = privkey.save_pkcs1()
    with open('private.pem', 'wb+')as f:
        f.write(pri)

#RSA加密，加密文本为base64的字符串
def encrypt(strs):
    with open('public.pem', 'rb') as publickfile:
        pub = publickfile.read()
        #print(pub)
    pubkey = rsa.PublicKey.load_pkcs1(pub)
    crypt_text = rsa.encrypt(strs.encode('utf8'), pubkey)
    encrypt_str = str(base64.b64encode(crypt_text), encoding="utf-8")
    return encrypt_str  # 加密后的密文

#RSA解密，输入字符为base64字符
def decrypt(decrypt_content):
    with open('private.pem', 'rb') as privatefile:
        pri = privatefile.read()
        #print(pri)
    privkey = rsa.PrivateKey.load_pkcs1(pri)
    strs = base64.b64decode(decrypt_content)
    decrypt_str = rsa.decrypt(strs, privkey).decode('utf-8')  # 注意，这里如果结果是bytes类型，就需要进行decode()转化为str
    return decrypt_str


if __name__ == '__main__':
    #create_keys()
    strs = 'helloello'
    enstr = encrypt(strs)
    destr = decrypt("bUH9fWD9cXLyL7HOtRlTrc6k8nPO7A/9VCyz7nqwtP/qRBFygsdOYXQGLLJvxAExV1HkF/V1xLJtSwLU177yx1NUTtIuZLRxXKORZBWD4nLhv6RdK6JVZvxEEWyTS9b4AUpSPyOmUh9clHK5YdW4gXgam93TgDhN9g7MEFe3TWM=")
    print(enstr)
    print(destr)
    strpri='MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCqWw1Cs3KLSEfYxZtVsc5BbBJDkDWnpgFDyZZnrcZgKcfBl4QQd94MF5KPqMCW5gFAfQTz33qdoRfdiMk9lMJFI5Hso6OSNCCts3PwuuKu+vVyd9tb1AllFycf+frO1TWT1/CAEYKz1cUjeGRbAd1Stk3I4tieDRXDiagy89QD9UfauxFoCmmhPxUQnuor1tj9YgqogbaAtOiV6Hd+08ByijqDkMb8Gk2abyIX0QXCajYOaxXTpcAgJ0y/9+eJwS6YtQglj1yoIeepoms/QlKLaQtbciamjqg2RM+/UOpTykapa95LWtIIQyBBELS4C8vIizi0xMoT70DRohHtQDFFAgMBAAECggEASZVSwQDp9SefgQ657eYDhVa8/NoJkwStfy5P/TCeg2MImaJstQBRtT9pWww1Q/jNiaF2xjA9smCnAlXXNvZOg7a9jU2RldFWXDErX0waJiLYaj1DTwLjm/rIN5OutjB6GAYqJ+doYW1w3cVe88B2bQe5yY/SqVJVmESZloijh6FvWOt41IJQXnocQrdKWL14Qs2++ByZsp3aX3She9gIalepaRkDCZYinzwROLnsyz7G+VoC33fCtfpjNKEMiELkEHA1cvQaGUoC668ZrR01+xJdVQj3UorezUPn35D3QrSenJA6hzkaFgKKCLsYY6eG1oQzk8kt/oiH4yS/RI3+gQKBgQD8tqCGkVkSxLadwHTKnxNlNaB699Oa9rWFI9q4Nm8enXtzvSX2+n+WSLOWy8OmrzLVT0W6TsiwBMU+NhlfBHUDjK8ojUFFHp80whp7XzRX7rLhV78Ifi85gTKQ73/lmwMb65ot6XMu/WvX2eXZTeETPusEVry7DVxPCeWJpstcjQKBgQCskjn/Wo2ORarFHWrKuQFfTbmDxbNoocSGcxybJCIt1qYObY2cN+0nVhXUQHwDLwmib8W2ciNUD7Bl3s1ppx//UboSpLLTX6gi9GP33hx+IvLJi4vwS0vsOmsmSnkp3/29quksf63R2+HPFWew2YvAejA6hFUXmfVQhYh+NQulmQKBgEwPd38pDgMdG82dGEnH2SGUI2n/Xzv71yRsZy8cixNO2NI78spEGNSVqx/O0mM/xe/3vQfXIIYB2Y/g3n//qmxYEnwFD8kK72YEqc3KGMoCeOpj64ZV3EbVgO+odfkYT7Y+khgRdG6HLcc4uPpPe1e8jyWuKX2vBsJvOxvUDM8dAoGARvDRsLxL5smS2vct2GpXOxNbiOTzzLZ7cH3Pq6Md/dP4bYPxq999uSb1UEy17iUfA8yI7WhcR4kvjaEati0CxVWWdWWJMcVn+/6VPebrtZquTdX0z/JUIqcdNx90UZeH2rlwo19VWCjY2eVFu/iiL5zLSLLvvAUJSHGsW514zTkCgYATvGxZON+ebnG/TbwE6uFZvPYXsjTp7uE98CcE12PVIMcBdK+kJubhgqb0e8tiDqZl303MbGcb/SWtom1dK/QIgU6nU4RfALO/+DSf2Ci6vv0txYb5Klpvb+K9vUbs5OKCOnkPNhHQjwrfHtlqF6lLlkOpIJO2Ps5KieoaoOmQ2Q=='
    fil22=base64.b64decode(strpri)
    with open('pury.pem', 'wb+')as f:
        f.write(fil22)


