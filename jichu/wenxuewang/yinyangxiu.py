import requests
from bs4 import BeautifulSoup
import time
import json

def perseUrl(url):
    send_headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
        'accept':'text/html,application/xhtml+xml,application/xml',
        'Connection': 'keep-alive'
    }
    html = requests.get(url, headers=send_headers)
    soup = BeautifulSoup(html.content, "lxml")
    return soup

def get_list(soup):
    contents = soup.find("div",{"id":"content"}).stripped_strings
    list_texts = []
    for text in contents:
        list_texts.append(text)
    return list_texts

def save(filename,title,contents):
    try:
        with open(filename,'a+') as file_object:
            file_object.write(title+"\n")
            for content in contents:
                file_object.write(content)
            file_object.write("\n")
        print("写入成功")
    except:
        print('保存异常')
        return None

def read_title_list(filename):
    try:
        with open(filename,'r') as file_object:
            lists = file_object.readlines()
            return lists
    except:
        print('读取异常')
        return None

url = "https://www.33yq.com/read/40935/24103711.shtml"
#path = "https://www.33yq.com/read/40935/24103711.shtml"
# soup = perseUrl(url)
# contents = get_list(soup)

# title=''
# save(filename,contents)
fileTitle = "loadfiles/yinyangxiuList.txt"
filename = "loadfiles/yinyangciqing.txt"
lists = read_title_list(fileTitle)
for li in lists[1000:1322]:
    chater = json.loads(li)
    title = chater['title']
    href = "https://www.33yq.com"+chater['href']
    soup = perseUrl(href)
    contents = get_list(soup)
    print("正在写入章节《%s》" % title)
    save(filename,title,contents)
