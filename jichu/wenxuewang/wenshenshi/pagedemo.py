import requests
from bs4 import BeautifulSoup
import time
import json

def perseUrl(url):
    send_headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
        'accept':'text/html,application/xhtml+xml,application/xml',
        'Connection': 'keep-alive'
    }
    html = requests.get(url, headers=send_headers)
    soup = BeautifulSoup(html.content, "lxml")
    return soup
def get_contents(soup):
    contents = soup.find("div",{"class":"content"}).stripped_strings
    list_texts = []
    i=1
    for text in contents:
        if i>2:
            list_texts.append(text)
        i=i+1
    return list_texts

def get_list(soup):
    lists = soup.find("div",{"id":"list"}).findAll("dd")

    for li in lists:
        chater = {}
        href  =  li.a.attrs["href"]
        title =  li.a.get_text()
        chater["title"]=title
        chater["href="]=href
        print(chater)

def save(filename,contents):
    try:
        with open(filename,'a+') as file_object:
            file_object.write("\n")
            for content in contents:
                file_object.write(content)
            file_object.write("\n")
    except:
        print('保存异常')
        return None


url = "https://www.33yq.com/read/40935/"
soup = perseUrl(url)

# for i in range(582723,583087):
#     path = url+"tc_"+str(i)+".html"
#     soup = perseUrl(path)
#     contents = get_contents(soup)
#     filename = "loadfiles/yinyangciqing.txt"
#     save(filename,contents)
#     print("写入第"+str(i)+"章："+path)
#     time.sleep(1)
# #tc_582723    tc_583086