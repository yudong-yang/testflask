import requests
from bs4 import BeautifulSoup
import time
import json

filename = "loadfiles/xiaoshuo.txt"
#解析URL
def perseUrl(url):
    send_headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
        'accept':'text/html,application/xhtml+xml,application/xml',
        'Connection': 'keep-alive'
    }
    html = requests.get(url, headers=send_headers)
    soup = BeautifulSoup(html.content, "lxml")
    return soup

#构建数据插入队列
def build_total_lists(curpage,path,start= 1,pagesize=30):
    if pagesize>50:
        print('输入的数值要在1--50之间')
        return None
    else:
        totallists=[]
        for i in range(int(start), int(pagesize)):
            url = path + str(i) + "/"
            soup = perseUrl(url)
            lists = get_page_list(soup)
            art_lists = build_article_lists(lists)
            save_lists = build_save_lists(curpage, art_lists)
            totallists.extend(save_lists)
            print('当前插入第：%d 页面' % i)
            time.sleep(2)
        return totallists

#获取文章分类：获取列表
def get_page_list(soup):
    content = soup.find("div",{"class":"fl_right"})
    lists = content.findAll("div",{"class":"tt"})
    return lists

#解析文章详情，标题，作者，链接等
def build_article(contents):
    article = {}
    title = contents.h3.get_text()
    href = contents.h3.a.attrs["href"]
    author = contents.find("p",{"class":"p1"}).get_text()
    desc = contents.find("p",{"class":"p2"}).get_text()
    article["title"]=str(title)
    article["href"]=str(href)
    article["author"]=str(author)
    article["desc"]=str(desc)
    return article

#获取所有列表
def getAllcatageList(filepath):
    with open(filepath,"r") as read_fi:
        catage_list = []
        lists = read_fi.readlines()
        for li in lists:
            lis = li.strip()
            json_obj = json.loads(lis)
            catage_list.append(json_obj)
        return catage_list



#获取指定类型的页面地址，首页地址
def get_url_bytype(file_path,type):
    lists_page = getAllcatageList(file_path)
    url = ''
    for li in lists_page:
        if str(li['title']) == type:
            url = li
            return url
    return url

def build_article_lists(content):
    article_lists = []
    for li in content:
        article_lists.append(build_article(li))
    return article_lists


#构建保存列表队列
def build_save_lists(curpage,article_lists):
    save_lists=[]
    for article in article_lists:
        li =(curpage["title"],article["title"],article["author"],article["href"],article["desc"])
        save_lists.append(li)
    return save_lists


#-----------------------------页面方法-----------------------#

#获取章节目录
def get_content_list(soup):
    try:
        contents = soup.find("div", {"class": "ml_list"})
        ulist = contents.findAll("li")
        linkList = []
        for li in ulist:
            linobj = {}
            title= li.get_text()
            link = li.find("a").attrs['href']
            linobj['title']=title
            linobj['link']=link
            linkList.append(linobj)
        return linkList
    except ellipsis:
        return None


#获取每章节内容文本
def get_content_text(soup):
    try:
        contents = soup.find("div", {"class": "novelcontent"})
        text = contents.find("p", {"class": "articlecontent"})
        return text.get_text()
    except:
        print("详情获取异常")
        return None


#保存每页到txt文本
def save(title,content):
    try:
        with open(filename,'a+') as file_object:
            file_object.write(str(title))
            file_object.write("\n")
            file_object.write(content)
            file_object.write("\n")
    except:
        print('保存异常')
        return None

# lists = xiaoshuoservice.build_lists()
# for li in lists[:2]:
#     print(li["title"])
#     url = "https://www.175wx.com/280/280587/"+li["link"]
#     soup = perseUrl(url)
#     content = get_content_text(soup)
#     save(li["title"],str(content).strip())
#     print("保存完成==="+li["title"])
#     time.sleep(2)
