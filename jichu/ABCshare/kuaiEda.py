from pyDes import des, CBC, PAD_PKCS5
import base64
from urllib import parse

class DES_tools:

    def __init__(self,key,iv):
        """
         构造函数
         ：param key 秘钥
         ：param iv 偏移量
         """
        self.key=key
        self.iv=iv

    def des_encrypt(self,encStr):
        """
        DES 加密
        :param s: 原始字符串
        :param key: 加密密钥8位
        :return: 加密后字符串，16进制
        """
        secret_key = self.key
        iv = self.iv  # 偏移量8位
        k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
        res = k.encrypt(encStr, padmode=PAD_PKCS5)
        msg = str(base64.b64encode(res), encoding="utf8")
        return msg


    def des_descrypt(self, desStr):
        """
        DES 解密
        :param s: 加密后的字符串，16进制
        :return:  解密后的字符串
        """
        secret_key = self.key
        iv = self.iv  # 偏移量8位
        k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
        de = k.decrypt(base64.decodebytes(desStr.encode("utf8")), padmode=PAD_PKCS5)
        return de.decode()
    def get_enstr(self,id='',projectID=None,**kwargs):
        str = 'id='+id+'&projectID='+projectID
        for key,value in kwargs.items():
            str=str+"&"+key+"="+value
        return str
    def build_url(self,path,id,projectId,**kwargs):
        dynastr = self.get_enstr(id=id,projectID=projectId,**kwargs)
        dynamicData=self.des_encrypt(dynastr)
        fullurl = path+"dynamicData="+parse.quote_plus(dynamicData)
        return fullurl


if __name__ == '__main__':
    destool = DES_tools('bankabc1','abchina1')
    s = 'id=5377&projectID=1212'
    encry_s = destool.des_encrypt(s)
    print(encry_s)
    descry_s = destool.des_descrypt(parse.unquote_plus('SenE71fLkfQbbOaM%2FgONFA%3D%3D'))
    print(descry_s)
    strs = parse.quote_plus("https://www.cnblogs.com/jinm+ao/p/6582755.html")
    strs2 = parse.quote("https://www.cnblogs.com/jinm+ao/p/6582755.html")
    print(strs)
    print(strs2)
    strs =destool.build_url('https://wx.abchina.com/webank/main-view/openTagForSZSes?id=GhzXW2O1DM4%3D&','4220',"asdsdf",shareCode='23232',channel='12121')
    print(strs)