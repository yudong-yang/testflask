import requests
import base64
import random
import time
def getABCshare(datas):
    url = 'https://enjoy.abchina.com/yx-web/wechatsharepage/getWechatHtmlparam?'
    params={
    "pageUrl":"https://enjoy.abchina.com/yx-web/wechatshare/getShareHtml",
    "shaWeUrl":"https://enjoy.abchina.com/yx-web/beanparadise/redirectTihrdInit",
    "title":datas['title'],
    "description":datas['description'],
    "imgUrl":datas['imgUrl'],
    "appId":"1EGPHB02V0330101007F00006D70334B",
    #"id":datas['id'],
    "shaBanImg":datas['shaBanImg'],
    "shaButImg":datas['shaButImg'],
    "shaBakImg":datas['shaBakImg'],
    "beanFlag":"101",
    "tempFlag":"1",
    "channel":"EKBS",
    "transNo":"EM000017",
    "shaTransNo":"EM0000015",
    "thirdUrl":datas['thirdUrl']
    }
    headers = {
        'Content-Type':'application/json',
        'Connection': 'keep-alive'
               }
    respon = requests.post(url,params=params,headers=headers)
    datas = respon.json()
    return datas

def getid():
    with open('idlists.txt', 'r') as ids:
        lists = ids.readlines()
        index = random.randint(0,len(lists)-1)
    return lists[index].replace('\n', '')

params = {}
params['title']='测试标题'
params['description']='测试分享描述'
params['imgUrl']='https://yun.duiba.com.cn/20191125/064cad5b-efe2-42c8-b122-561d1df886e2.png'
params['id']=getid()
params['shaBanImg']='https://yun.dui88.com/DS-tech/yyd_tech/6f020f80-fead-4467-ad3d-d1c0d7e8c7ca.jpg'
params['shaButImg']='https://yun.dui88.com/DS-tech/yyd_tech/6f020f80-fead-4467-ad3d-d1c0d7e8c7ca.jpg'
params['shaBakImg']='https://yun.dui88.com/DS-tech/yyd_tech/6f020f80-fead-4467-ad3d-d1c0d7e8c7ca.jpg'
#linkurl = "https://76736.activity-1.m.duiba.com.cn/customShare/share?id=4308&projectID=pe94c3c75"
linkurl = "https://activity.m.duiba.com.cn/customShare/share?id=4358"
thridUrl=base64.b64encode(linkurl.encode()).decode()
params['thirdUrl']=thridUrl


jsons = getABCshare(params)
print()
if jsons['status']=='success':
        print(jsons['result']['share_url'])
