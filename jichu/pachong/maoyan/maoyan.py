import requests
from bs4 import BeautifulSoup


def get_content(Tags):
    data = Tags
    texts = {}
    index = data.find("i", {"class": "board-index"}).text.strip()
    src = data.find("img", {"class": "board-img"}).attrs['data-src']
    name = data.find("p", {"class": "name"}).a['title']
    href = data.find("p", {"class": "name"}).a['href']
    star = data.find("p", {"class": "star"}).get_text().strip()
    releasetime = data.find("p", {"class": "releasetime"}).get_text().strip()
    score = data.find("p", {"class": "score"}).get_text().strip()
    texts["index"] = index
    texts["src"] =src
    texts["name"] = name
    texts["href"] = href
    texts["star"] = star
    texts["releasetime"] = releasetime
    texts["score"] =score
    return texts

url0 = "https://maoyan.com"
headers = {
'Host': 'maoyan.com',
'Referer': 'https://maoyan.com/',
'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
}
url = 'https://maoyan.com/board'
req = requests.session()
res = req.get(url0,headers=headers)
response = req.get(url,headers=headers,cookies = res.cookies)
soup= BeautifulSoup(response.content,"html.parser")
content = soup.find("dl",{"class":"board-wrapper"}).findAll('dd')
#content = soup.select(".board-wrapper")
for i in content:
    txt = get_content(i)
    print(txt)







