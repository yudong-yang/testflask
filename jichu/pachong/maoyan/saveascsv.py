import csv


datas = [{'index': '1',
         'src': 'https://p0.meituan.net/movie/ce4da3e03e655b5b88ed31b5cd7896cf62472.jpg@160w_220h_1e_1c',
         'name': '霸王别姬',
         'href': '/films/1203',
         'star': '主演：张国荣,张丰毅,巩俐',
         'releasetime': '上映时间：1993-07-26',
         'score': '9.5'},{'index': '2', 'src': 'https://p0.meituan.net/movie/283292171619cdfd5b240c8fd093f1eb255670.jpg@160w_220h_1e_1c', 'name': '肖申克的救赎', 'href': '/films/1297', 'star': '主演：蒂姆·罗宾斯,摩根·弗里曼,鲍勃·冈顿', 'releasetime': '上映时间：1994-09-10(加拿大)', 'score': '9.5'}
]




def build_requrl(pre_url,sumPages):
    url_list=[]
    url = pre_url
    for i in range(sumPages):
        if i>0:
            offset = i*10
            url = pre_url+'?offset='+str(offset)
        url_list.append(url)
    return url_list



preurl='https://maoyan.com/board/4'
counts = 10
lists = build_requrl(preurl,counts)
print(lists)