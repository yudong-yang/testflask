import requests
from requests.cookies import RequestsCookieJar
from bs4 import BeautifulSoup
import csv

def get_content(data):
    texts = {}
    try:
        index = data.find("i", {"class": "board-index"}).get_text().strip()
        src = data.find("img", {"class": "board-img"}).attrs['data-src']
        name = data.find("p", {"class": "name"}).a['title']
        href = data.find("p", {"class": "name"}).a['href']
        star = data.find("p", {"class": "star"}).get_text().strip()
        releasetime = data.find("p", {"class": "releasetime"}).get_text().strip()
        score = data.find("p", {"class": "score"}).get_text().strip()
        texts["index"] = index
        texts["src"] =src
        texts["name"] = name
        texts["href"] = href
        texts["star"] = star
        texts["releasetime"] = releasetime
        texts["score"] =score
    except:texts=None
    return texts


def save_file(datas):
    with open('data.csv', 'w') as cs:
        try:
            cswriter = csv.writer(cs)
            cswriter.writerow(["排名", "图片", "标题", "链接", "主演", "上映时间", "评分"])
            for data in datas:
                cswriter.writerow(
                    [data['index'], data['src'], data['name'], data['href'], data['star'], data['releasetime'], data['score']])
            print('写入完成！')
        except :print('写入异常')

def build_datalists(soup):
    try:
        contents = soup.find("dl", {"class": "board-wrapper"}).findAll('dd')
        text_lists = []
        for content in contents:
            text = get_content(content)
            text_lists.append(text)
        return text_lists
    except:return None



def build_requrl(pre_url,sumPages):
    url_list=[]
    url = pre_url
    for i in range(sumPages):
        if i>0:
            offset = i*10
            url = pre_url+'?offset='+str(offset)
        url_list.append(url)
    return url_list


surl = "https://maoyan.com"
url = 'https://maoyan.com/board/4'
headers={
'Host': 'maoyan.com',
'Referer': 'https://maoyan.com/',
'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
}

url_list = build_requrl(url,10)
sum_list = []
for li in url_list:
    sour = requests.get(surl)
    cookies_dict = requests.utils.dict_from_cookiejar(sour.cookies)
    #设置cookies
    reqcookies = RequestsCookieJar()
    for k ,v in cookies_dict:
        reqcookies.set(k,v)
    response=requests.get(li,headers=headers,cookies = reqcookies)
    soup= BeautifulSoup(response.content,"html.parser")
    data_lists=build_datalists(soup)
    sum_list.extend(data_lists)


save_file(sum_list)

























