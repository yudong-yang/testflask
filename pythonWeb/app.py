from flask import session,redirect,render_template,request,url_for
from control.citycontrol import citys
from control.stucontrol import stu
from control.uploadfile import upfile
from control.Hjcontrol import huiju
from config import app
from model.models import User
from control.formscontrol import LoginForms
#此处没有再定义APP对象，但是也可以使用，所以全局只有一个APP对象

app.register_blueprint(citys)
app.register_blueprint(stu)
app.register_blueprint(upfile)
app.register_blueprint(huiju)

@app.before_request
def before_action():
    print(request.path)
    if  request.path not in ['/login','/loginout','/favicon.ico','/huiju/index','huiju/login']:
        if session.get('username')==None:
            print('跳转登录')
            session['newurl'] = request.path
            return redirect(url_for('login'))



@app.route('/')
def indexpage():
    return render_template('index.html')
@app.route('/forms')
def forms():
    forms = LoginForms()
    return forms.prassvord

@app.route('/favicon.ico')
def get_fav():
    return app.send_static_file('img/favicon.ico')


@app.route('/loginout')
def loginout():
    if session.get('username')=='admin':
        session.pop('username',None)
    return render_template('login.html')

@app.route('/login',methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        name = request.form['username']
        user = User.query.filter_by(s_name=name).first()
        print(user)
        if user is not None:
            session['username'] = request.form['username']
            if 'newurl' in session:
                newurl = session['newurl']
                print('newUrl='+newurl)
                session.pop('newurl', None)
                return redirect(newurl)
            else:
                return redirect('/index')

        else:
            error = '账号密码错误：账号：admin/密码：111'
    return render_template('login.html', error=error)


@app.route('/index')
def index():
    return render_template('index.html', username=session['username'])


#-------------------测试发货接口--------------------
@app.route('/cs01/sendgoods')
def sendgoods():
    return 'ok'



@app.route('/check')
def check():
    return render_template('checkall.html')



app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == '__main__':
    # db.drop_all()
    # db.create_all()
    app.run('0.0.0.0')
