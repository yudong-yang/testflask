from config import *

class Student(db.Model):
    __tablename__ = "student"
    s_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    s_name = db.Column(db.String(50), nullable=False)
    s_age = db.Column(db.Integer, nullable=False)

    def __init__(self,name,age):
        self.s_age=age
        self.s_name=name

class User(db.Model):
    __tablename__ = 'Users'
    s_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    s_name = db.Column(db.String(50), nullable=False)
    s_password = db.Column(db.String(50), nullable=False)

    def __init__(self,name,password):
        self.s_name=name
        self.s_password=password

class Contents(db.Model):
    __tablename__ = "contents"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(50), nullable=False)
    url = db.Column(db.String(50), nullable=False)
    content = db.Column(db.Text, nullable=False)

    def __init__(self,title,url,content):
        self.title=title
        self.url=url
        self.content=content


class Citys(db.Model):
    __tablename__ = "citys"
    id = db.Column(db.Integer, primary_key=True)
    count = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(50), nullable=False)
    pinyinFull = db.Column(db.String(50), nullable=False)
    pinyinShort = db.Column(db.String(50), nullable=False)

    def __init__(self,id,count,name,pinyinFull,pinyinShort):
        self.id=id
        self.count=count
        self.name=name
        self.pinyinFull=pinyinFull
        self.pinyinShort=pinyinShort


