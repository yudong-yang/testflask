--创建数据库不再列出

-- auto-generated definition
create table contents
(
  id      INT(10) auto_increment
    primary key,
  title   VARCHAR(255) null,
  url     VARCHAR(255) null,
  content TEXT(65535)  null
);


-- auto-generated definition
create table student
(
  s_id   INT(10) auto_increment
    primary key,
  s_name VARCHAR(20) null,
  s_age  INT(10)     null
);

create unique index s_id
  on student (s_id);

