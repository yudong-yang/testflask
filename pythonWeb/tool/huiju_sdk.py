import hashlib
import json
import requests
from datetime import datetime
import config

class OpenPlatformDemo(object):

    def __init__(self):
        self.appID = config.Huiju_appID # AppID
        self.publicKey = config.Huiju_publicKey # 公钥
        self.privateKey = config.Huiju_privateKey  # 私钥

    def mallcoo_post(self, url, json_data):
        '''
        :param url: 请求url
        :param post_data: 请求参数（json格式）
        :return: Response object
        '''

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')  # 时间戳

        encryptString = "{publicKey:" + self.publicKey + ",timeStamp:" + timestamp + ",data:" + json.dumps(
            json_data) + ",privateKey:" + self.privateKey + "}"  # 待加密字符串
        sign = hashlib.md5(encryptString.encode(encoding='utf-8')).hexdigest()[8:24].upper()  # 16位MD5加密（大写）

        headers = {
            'Content-Type': 'application/json;charset=utf-8',
            'AppID': self.appID,
            'PublicKey': self.publicKey,
            'TimeStamp': timestamp,
            'Sign': sign
        }

        response = requests.post(url, json=json_data, headers=headers)

        return response