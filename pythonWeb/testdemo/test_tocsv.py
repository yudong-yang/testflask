import pandas as pd
import os

# 读取一个目录里面的所有文件：
def read_path(path):
    dirs = os.listdir(path)
    return dirs

def getpath(dir):
    # pwd = os.getcwd()
    # father_path=os.path.abspath(os.path.dirname(pwd)+os.path.sep+".")
    file_path = '/Users/yangyudong/Desktop'#桌面路径地址，地址可以根据自己需要修改
    full_path=file_path+'/'+dir+'/'
    return full_path


#数据源Excel如果有多个sheet，那么则需要一一遍历输出
def xlsdata_to_csv(xlsx_data,tag_path):
    for K, V in xlsx_data.items():
        tag_file = tag_path +  K + '.csv'
        if not V.empty:
            V.to_csv(tag_file, encoding='utf_8_sig')


def main():
    source_path = getpath('test2')#存放excel文件
    tag_path = getpath('test3')#输出csv的文件
    dir = read_path(source_path)
    for i in dir :
        source_file = source_path+i
        xlsx_data = pd.read_excel(source_file, sheet_name=None)
        csv_path = tag_path+i.split('.x')[0]#因为文件包含两个.所以分割时候做了特殊处理
        xlsdata_to_csv(xlsx_data,csv_path)

if __name__ == '__main__':

    main()