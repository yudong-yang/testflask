from flask import Blueprint,redirect,render_template,request,url_for
from config import *


upfile=Blueprint('uploadfile',__name__)

@upfile.route('/file')
def file():
    return render_template('uploadfile.html')