from flask import Blueprint,redirect,render_template
from service.service import *
from config import db

citys=Blueprint('citycontrol',__name__)

@citys.route('/city/index')
def city_index():
    count = Citys.query.count()
    if count==0:
        url = build_request()
        json_data = get_jsondata(url)
        list_citys = build_citys(json_data)
        db.session.add_all(list_citys)
        db.session.commit()
    return redirect('/city/listcity/0')


@citys.route('/city/listcity/<int:pageno>')
def list_city(pageno):
    try:
        count = Citys.query.count()
        page_size = 10
        page_count = int(count / 10)
        if pageno >= page_count:
            pageno = page_count
        page_list = range(page_count)
        pagination = Citys.query.paginate(pageno, per_page=page_size, error_out=False)
        results = pagination.items
        return render_template('citylist.html', results=results, cur_page=pageno, page_count=page_count,
                               page_list=page_list)
    except Exception:
        print("发生异常==:", Exception)
        return "出错了！"


@citys.route('/city/delcity/<int:id>')
def delcity(id):
    try:
        city = Citys.query.filter_by(s_id=id).first()
        db.session.delete(city)
        db.session.commit()
        return 'success'
    except Exception:
        print("发生异常==:", Exception)
        return 'fail'