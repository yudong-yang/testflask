from wtforms import Form,StringField,SubmitField,BooleanField,PasswordField
from  wtforms.validators import DataRequired,Length
class LoginForms(Form):
    username =StringField("Username",validators=[DataRequired()])
    prassvord =PasswordField("Password",validators=[DataRequired()])
    remember = BooleanField("Remember me")
    submit = StringField("Login in")