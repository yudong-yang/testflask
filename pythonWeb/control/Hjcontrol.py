from flask import Blueprint,redirect,render_template,request
from service.huiju_service import *

huiju = Blueprint('Hjcontrol',__name__)

@huiju.route('/huiju/login')
def huiju_login():
    data = request.args.to_dict()
    Ticket = data["Ticket"]
    UserToken = get_userToken(Ticket)
    UserInfo = get_userInfo(UserToken)
    redirectUrl = "https://activity.m.duiba.com.cn/sign/treasure/index?id=138863168329172"
    avatar=UserInfo["Avatar"]
    nickName = UserInfo["NickName"]
    autoUrl = build_autologin(UserInfo["Mobile"],UserInfo["Score"],redirect=redirectUrl,dcustom='nackName=%s&avatar=%s'%(nickName,avatar))
    return redirect(autoUrl)

@huiju.route('/huiju/index')
def huiju_index():
    #https://m.mallcoo.cn/a/open/User/V2/OAuth/CardInfo/?AppID=5f0d8e504bae7051c88ad953&PublicKey=-Ham7p&CallbackUrl=http://10.10.95.80:5000/huiju/login
    url = 'https://m.mallcoo.cn/a/open/User/V2/OAuth/CardInfo/?'
    APPID = config.Huiju_appID
    PublicKey = config.Huiju_publicKey
    CallbackUrl='http://0.0.0.0:5000/huiju/login'
    full_url = url+'APPID='+APPID+'&PublicKey='+PublicKey+'&CallbackUrl='+CallbackUrl
    return redirect(full_url)