from flask import Blueprint,redirect,render_template,request,url_for
from config import db
from model.models import Student

stu=Blueprint('stucontrol',__name__)

@stu.route('/liststu')
def liststu():
   results = Student.query.all()
   return render_template('list.html', results=results)


@stu.route('/pagestu')
def pagestu():
   results = Student.query.all()
   return redirect(url_for('stucontrol.student',pageno=0))

@stu.route('/student/<int:pageno>')
def student(pageno):
    try:
        count = Student.query.count()
        page_size = 10
        page_count = int(count / 10)
        if pageno >= page_count:
            pageno = page_count
        page_list = range(page_count)
        pagination = Student.query.paginate(pageno, per_page=page_size, error_out=False)
        results = pagination.items
        return render_template('pagestudent.html', results=results, cur_page=pageno, page_count=page_count,
                               page_list=page_list)
    except Exception:
        print("发生异常==:", Exception)
        return "出错了！"



@stu.route('/insert',methods=['post'])
def insert():
    data = request.form.to_dict()
    try:
        if data['s_name']!=''and data['s_age']!='':
            stu = Student(data['s_name'],data['s_age'])
            db.session.add(stu)
            db.session.commit()
            return redirect(url_for('stu.liststu'))
        return '插入数据为空'
    except Exception :
        print("发生异常==:", Exception)
        return "数据库插入异常"


@stu.route('/bachsave/<int:num>')
def bachsave(num):
    list_stu= []
    for i in range(1,num):
        stu = Student('小米',23)
        list_stu.append(stu)
    db.session.add_all(list_stu)
    db.session.commit()
    print(url_for('.student',pageno=1))
    return redirect(url_for('.student',pageno=1))


@stu.route('/delete',methods=['post'])
def deltet():
    try:
        id=int(request.form.to_dict()['stuid'])
        print(id)
        stu =Student.query.filter_by(s_id=id).first()
        db.session.delete(stu)
        db.session.commit()
        print('删除成功')
        return 'success'
    except:
        print('删除失败')
        return 'fail'

@stu.route('/batchdel',methods=['post'])
def batchdel():
    try:
        lists=request.form.getlist('ids[]')
        for id in lists:
            stu = Student.query.filter_by(s_id=id).first()
            db.session.delete(stu)
        db.session.commit()
        # stu = Student.qurey.filter_by(Student.s_id.in_(lists)).delete(synchronize_session=False)
        # print(lists)
        return 'success'
    except AttributeError as e:
        print(e)
        return 'fail'

@stu.route('/show/<id>')
def show(id):
    cur_page=request.args.get("cur_page")
    stu = Student.query.filter_by(s_id=id).first()
    return render_template('update.html', data = stu, cur_page =cur_page)

@stu.route('/update',methods=['post'])
def update():
    data = request.form.to_dict()
    id=int(data['s_id'])
    cur_page=int(data['cur_page'])
    stu = Student.query.filter_by(s_id=id).first()
    stu.s_name=data['s_name']
    stu.s_age=data['s_age']
    db.session.commit()
    return redirect(url_for('.student', pageno=cur_page))

