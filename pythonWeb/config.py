from flask_sqlalchemy import SQLAlchemy
from flask import Flask


UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

DUIBA_APPKEY="jlg88lyxz7siqtmr"
DUIBA_APPSECRET="1x0eap95f4xfi77uaptrnwh9ewzvlm"

Huiju_appID = '5f0d8e504bae7051c88ad953'  # AppID
Huiju_publicKey = '-Ham7p'  # 公钥
Huiju_privateKey = '152fe116c89bc527'  # 私钥

app = Flask(__name__) #数据初始化使用到的APP对象，全局使用，在app.py里面调用时候，直接引用这里的就可以了，不需要再单独定义

#链接数据库的两种方式，一种通过IP方式连接，一种通过tcp形式链接
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:root@10.10.65.220:3306/test?charset=utf8"
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:root@172.16.160.227:3306/test?charset=utf8"
#通过tcp格式链接数据库
#app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:root@server.natappfree.cc:42231/test?charset=utf8"
app.config['SQLALCHEMY_ECHO'] = False
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# 文件上传操作
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

db = SQLAlchemy(app)

#tcp://server.natappfree.cc:33874