from tool.huiju_sdk import OpenPlatformDemo
from duiba import duibaSDK as sdk
import config
##方法处理
def get_userToken(Ticket):
        userToken = None
        url = ' https://openapi10.mallcoo.cn/User/OAuth/v1/GetToken/ByTicket/'
        json_data = {
            "Ticket": Ticket
        }
        try:
            demo = OpenPlatformDemo()
            response = demo.mallcoo_post(url, json_data)
            #print(response.status_code, response.text, sep='\n')
            if response.status_code==200 and response.json()["Code"]==1:
                userToken = response.json()["Data"]["UserToken"]
            else:print("获取UserToken失败")
        except:print("获取UserToken信息异常")
        return userToken

def get_userInfo(UserToken):
        url="https://openapi10.mallcoo.cn/User/AdvancedInfo/v1/Get/ByToken/"
        json_data={
            "UserToken":UserToken
        }
        userInfo = {}
        try:
            demo = OpenPlatformDemo()
            response = demo.mallcoo_post(url, json_data)
            #print(response.status_code, response.text, sep='\n')
            if response.status_code == 200 and response.json()["Code"] == 1:
                userInfo["Mobile"]=response.json()["Data"]["Mobile"]
                userInfo["NickName"]=response.json()["Data"]["NickName"]
                userInfo["Avatar"]=response.json()["Data"]["Avatar"]
                userInfo["Score"]=response.json()["Data"]["Score"]
            else:print("获取UserInfo失败")
        except: print("获取UserInfo异常")
        return userInfo

def add_credits(Mobile,Score,TransID,ScoreEvent,Reason):
            url = "https://openapi10.mallcoo.cn/User/Score/v1/Subtract/ByMobile/"
            json_data={
              "Mobile": Mobile,
              "Score": Score,
              "TransID": TransID,
              "ScoreEvent": ScoreEvent,
              "Reason":Reason
            }
            demo = OpenPlatformDemo()
            response = demo.mallcoo_post(url, json_data)
            print(response.status_code, response.text, sep='\n')


def get_coupen_list(PageIndex,PageSize):
            url = "https://openapi10.mallcoo.cn/Coupon/PutIn/v3/GetAll/"
            json_data = {
                "PageIndex": PageIndex,
                "PageSize": PageSize
            }
            demo = OpenPlatformDemo()
            response = demo.mallcoo_post(url, json_data)
            #print(response.status_code, response.text, sep='\n')


def build_autologin(uid,credirs,appKey=config.DUIBA_APPKEY,appSecret=config.DUIBA_APPSECRET,**keywords):
    tool = sdk.CreditsTool(appKey, appSecret)  # 大富翁
    url = "https://activity.m.duiba.com.cn/autoLogin/autologin?"
    params = {}
    params["uid"]=uid
    params["credirs"]=credirs
    for key, value in keywords.items():
        params[key]=value
    autologinUrl =tool.buildUrlWithSign(url=url,params=params)
    return autologinUrl



add_credits("18258149680","1","e51c1c060eae48d8a3f85037eabb0841323","2000","积分变动")