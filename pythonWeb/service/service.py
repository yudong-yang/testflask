from duiba import duibaSDK as sdk
import requests
import json
from model.models import Citys

tool =sdk.CreditsTool("3RWB6Jws3YBMmDsgP8mrw6FJ7kx6", "2C6zguqBLAVamii2AFtgGQ2b27UK"); #万达

def build_request():
    url = 'https://duiba-api-prd-mx.wandafilm.com/duiba/city.api'
    params = {}
    params['json'] = True
    urls = tool.buildUrlWithSign(url,params)
    return urls

def get_jsondata(url):
    response =requests.get(url)
    data = response.content
    json_obj = json.loads(data)
    return json_obj

def build_citys(json_obj):
    lists = []
    datas = json_obj['data']['city']
    for data in datas:
        city = Citys(data['id'],data['count'],data['name'],data['pinyinFull'],data['pinyinShort'])
        lists.append(city)
    return lists
