from flask import Flask,request,render_template,redirect,url_for,Markup
from jinja2 import escape
import tools
import dao
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/watchList')
def watchList():
    tag  = request.args.get('tag')
    return render_template('watchList.html',user = dao.user,movies = dao.movies,tag = tag)

@app.route('/getparam')
def getparam():
    params = request.args.to_dict()
    appsecret = dao.appconfig.get('prod_appsecret')
    if tools.signverify(params,appsecret):
        return '验证通过'
    else:
        return '验证失败'

@app.route('/index')
def index():
    tag = request.args.get('name')
    print(tag)
    return redirect(url_for('watchList',tag = tag ))


@app.route('/test')
def test():
    print(request.headers)
    print(request.cookies)
    return 'index'

@app.route('/getcode')
def getconfig():
    token_data = tools.build_token_datas()
    post_data = tools.build_get_post_data(token_data)
    frontToken = tools.get_frontToken(post_data)
    url = 'http://activity.m.duiba.com.cn/customShare/share?id=3906'
    configtext = tools.build_config_data(post_data,url,frontToken)
    return configtext

@app.route('/sayHello')
def sayHello():
    text = Markup('<h1> sayHello ! </h1>')
    return render_template('index.html',text = text)
    return response
if __name__ == '__main__':
    app.run('0.0.0.0')
