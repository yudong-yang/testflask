import random
import requests
import json
import hashlib
import time


appId='7f701174bf8a4cb681cb58b9db995429'
secret='51a1d4d30bf948eb84b2b3cc1381b5da'

def sign(params):
    '对参数进行签名验证'
    sign_str=''
    keys=sorted(params.keys())
    for key in keys:
        sign_str=sign_str + key +'=' + str(params[key])+'&'
    sign = hashlib.sha256(sign_str[:-1].encode("utf8")).hexdigest()
    return sign

#sha256签名方法
def sign_256(params,appsecret):
    '对参数进行签名验证'
    sign_str=''
    keys=sorted(params.keys())
    for key in keys:
        sign_str=sign_str + (params[key])
    sign_str = sign_str+appsecret
    sign = hashlib.sha256(sign_str.encode("utf8")).hexdigest()
    return sign

def signverify(params,appsecret):
    ver_sign = ''
    if params.get('sign')!=None:
        ver_sign=params.get('sign')
        params.pop('sign')
    if sign_256(params,appsecret)==ver_sign:
        print('签名成功')
        return True
    else:
        print('签名失败')
        return False



def build_nonceStr():
    strs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'
    sub_str = ''
    for i in range(0,16):
        index = random.randint(0, len(strs)-1)
        sub_str = sub_str+strs[index]
    return sub_str


def get_frontToken(post_data):
    url = 'https://open.95516.com/open/access/1.0/frontToken'
    jsons = json.dumps(post_data)
    texts = requests.post(url, data=jsons)
    frontToken=None
    if texts.json()['resp']=='00':
        frontToken = texts.json()['params']['frontToken']
    return frontToken


def get_backendToken(post_data):
    url = 'https://open.95516.com/open/access/1.0/backendToken?'
    jsons = json.dumps(post_data)
    texts = requests.post(url, data=jsons)
    backendToken=None
    if texts.json()['resp']=='00':
        backendToken = texts.json()['params']['backendToken']
    return backendToken

def build_token_datas():
    token_data = {}
    token_data['appId'] = appId
    token_data['nonceStr'] = build_nonceStr()
    token_data['timestamp'] = int(time.time())
    token_data['secret'] = secret
    return token_data

def build_get_post_data(token_data):
    signature = sign(token_data)
    if token_data.get('secret')!=None:
        token_data.pop('secret')
    token_data['signature']=signature
    return token_data

def build_config_data(post_data,url,frontToken):
    if post_data.get('secret')!=None:
        post_data.pop('secret')
    post_data['url'] = url
    post_data['frontToken'] = frontToken
    signature = sign(post_data)
    post_data['signature'] = signature
    return post_data


