from flask import Flask ,url_for,request, redirect,jsonify,render_template
import time
app = Flask(__name__)

@app.before_request
def before():
    print(request.path.find('/say'))
    print('执行开始时间：%s' % time.strftime('%H:%M:%S'))

# @app.after_request
# def appafter(gobackint):
#     print('执行开始时间：%s' % time.strftime('%H:%M:%S'))
#     return "3323423"


@app.route('/')
def hello_world():
    return 'Hello World!'
@app.route('/hello/<name>')
def hello_byname(name):
    return 'Hello %s' % name
@app.route('/sayHello')
def say_hello():
   name = request.args.get("name","Jack")
   return '<H2> hello %s' % name
@app.route('/gobackint/<int:years>')
def gobackint(years):
    return '<H2> welcome to %d </H2>' % (2020-years)
@app.route('/backto/<string:name>')
def backtotag(name):
    return '<H2> welcome to %s </H2>' % name

@app.route('/showcity/<any(hangzhou, beijing, shanghai, shenzhen):city>')
def showcity(city):
    return '<H2> welcome to %s </H2>' % city
@app.route('/redirectag')
def redirectag():
    tag = request.args.get("tag")
    #print(url_for('gobackint',years=tag))
    return redirect(url_for('gobackint',years=tag))

@app.route('/jsondata')
def testjson():
    return 'I M A TEAPOT' ,418

@app.route('/userlist')
def getUserlist():
    user = {'username': 'Grey Li', 'bio': 'A boy who loves movies and music.', }
    movies = [{'name': 'My Neighbor Totoro', 'year': '1988'}, {'name': 'Three Colours trilogy', 'year': '1993'},
              {'name': 'Forrest Gump', 'year': '1994'}, {'name': 'Perfect Blue', 'year': '1997'},
              {'name': 'The Matrix', 'year': '1999'}, {'name': 'Memento', 'year': '2000'},
              {'name': 'The Bucket list', 'year': '2007'}, {'name': 'Black Swan', 'year': '2010'},
              {'name': 'Gone Girl', 'year': '2014'}, {'name': 'CoCo', 'year': '2017'}, ]
    return render_template('watchlist.html', user=user, movies=movies)


@app.route('/index')
def index():
    return 'ok'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug='True')
